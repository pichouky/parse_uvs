# -*- coding: utf-8 -*-
from __future__ import unicode_literals

#########################################
################# Init ##################
#########################################
####################
### Some imports ###
####################
import sys          # System call
import re           # Regex
import getopt       # Manage CLI options
import os           # Execute shell script
####################
##### Variables ####
####################
# Regex to match an UV code (and ONLY UV code)
regex_uv = '^[A-Z]{2,3}[0-9]{0,2}$|^[A-Z]{1}[0-9]{1}[A-Z]{1}[0-9]{1}|^[A-Z]{2}[0-9]{1}[A-Z]{1}$'
### Default variables, can be override
# Input txt file (generate with pdftotext)
input_file = 'eval.pdf'
# Output CSv file
output_file = 'stats.csv'

#########################################
############### Functions ###############
#########################################
def match(regex,word):
    """
    Try to match a string with a regex.
    Input:
        - regex (str): Regex
        - word (str): String to analyse
    Output:
        - (Boolean) : True is string match regex, False otherwise
    """
    r = re.compile(regex)
    m = r.match(word)
    if m is None:
        return False
    else:
        return True

def extract_success(line):
    """
    Parse the stats line of an UV to return the percentage of success
    Input :
        - line (str) : line to parse
    Output:
        - perc (float) : percentage of success
        OR
        None if not found
    """
    try:
        perc_txt = re.search("reçus \(([0-9]{1,3}.*?)%",line).group(1)
        perc = float(perc_txt.replace(",","."))
    except ValueError:
        return None
    except AttributeError:
        return None
    return perc

def write_csv(uvs):
    """
    Write the result in a CSV file.
        semester_code,uv_code,rate
        A16,BC01,99.4537
    Input:
        uvs (list) : list of CSV lines
    """
    # Generate string
    csv_str = ""
    for uv in uvs:
        csv_str += unicode(uv[0]) + ',' + unicode(uv[1]) + ',' + unicode(uv[2]) + '\n'
    # Write file
    fd = open(output_file, 'w')
    fd.write(csv_str.encode('utf-8'))
    fd.close()

def usage():
    sys.stderr.write("""
    Parse and extract success rate for all UV.

    USAGE: parse_uvs.py [-i input_file] [-o output_file] semester_code
    ARGUMENTS:
        semester_code               Semester code (eg. A16)

    OPTIONS:
        -h, --help                  Show this help message and exit
        -i                          Path for the input PDF file
        -o                          Path for the CSV output file
""")

#########################################
################# Main ##################
#########################################
def main():
    ### Convert PDF to TXT
    os.popen('pdftotext -layout -eol unix -nopgbrk ' + str(input_file))
    ### Open and read the PDF file. Create an array of lines
    fd = open('.'.join(input_file.split('.')[:-1])+'.txt','r')
    data = fd.read().decode('utf-8').split('\n')
    fd.close()

    uvs = []
    ### Extract UV lines
    for i,entry in enumerate(data):
        if "-" in entry:
            ## Extract and test UV code
            uv_code = entry.replace(' ','').split('-')[0]
            if match(regex_uv,uv_code):
                # Get the percentage line
                if '%' in data[i+1]:
                    res = extract_success(data[i+1])
                elif '%' in data[i+2]:
                    res = extract_success(data[i+2])
                else:
                    print >> sys.stderr, ("Impossible to parse result line for UV " + unicode(uv_code)).encode('utf-8')
                    continue
                ## Add information to list
                if res is not None:
                    uvs.append([semester_code,uv_code,res])
                else:
                    print >> sys.stderr, ("Impossible to extract success rate for UV " + unicode(uv_code)).encode('utf-8')
    write_csv(uvs)

#########################################
############## Parameters ###############
#########################################
if __name__ == "__main__":

    ### Available options
    short_options = 'hi:o:'
    long_options  = ['help']

    ### Get CLI arguments
    try:
        opts, args = getopt.getopt(sys.argv[1:], short_options, long_options)
    except getopt.GetoptError, err:
        sys.stderr.write("%s\n" % err)
        sys.exit(1)
    for o,a in opts:
        if o in [ '-h', '--help' ]:
            usage()
            sys.exit(0)
        elif o in [ '-i']:
            input_file = a
        elif o in [ '-o']:
            output_file = a

    ### Get device name
    if len(args) == 1:
        semester_code = args[0]
    elif not args:
        sys.stderr.write("ERROR: No semester code given.\n")
        usage()
        sys.exit(1)
    else:
        sys.stderr.write("Not managed error.\n")
        sys.exit(1)

    main()
