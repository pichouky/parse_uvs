# Parse UVs

Parse PDF results of UVs evalutation to extract some data.

## Quickstart

Only need `python` and `pdftotext` to work. Just running `parse_uvs.py` passing semester code and some optional parameters.
```
python parse_uvs.py -i eval_enseig_P2014_sans.pdf -o stats_P14.csv P14
```
